// Required packages to use React
import React from 'react'; //Creating React components and stuff
import ReactDOM from 'react-dom'; //DOM manipulation

// const Profile = () => {

// 	return <p>I am a React Developer.</p>;

// };

// const Person = () => {
// 	return (	
// 		<h1>
// 			<p>Hello, I'm Hiro! This is my first React component.</p>
// 		</h1>
// 	)
// };

// function HelloWorld(){
// 	// Return a JSX element with a simple text
// 	// JSX--> JavaScript and XML
// 	// const message = React.createElement('p',{}, "Hello, I'm Hiro! This is my first React component.")
// 	// return React.createElement('h1', {}, message);

// 	return(
// 		<React.Fragment>
// 			<Person />
// 			<Profile />
// 		</React.Fragment>
// 	);
// }

const BookList = () => {
	return(
		<>
			<Book 
			image="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1367545443l/157993.jpg" 
			alternate="A cover page of a book"
			title="The Little Prince" 
			author="Antoine de Saint-Exupery"
			/>
			<Book 
			image="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1166859639l/18135._SY475_.jpg" 
			alternate="Romeo and Juliet book"
			title="Romeo and Juliet" 
			author="William"
			/>
			<Book 
			image="https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1490528560l/4671._SY475_.jpg" 
			alternate="The Great Gatsby book"
			title="The Great Gatsby" 
			author="Scott Fitzgerald"
			/>
		</>
	)
};

const Book = ({image, alternate, title, author}) => {
	return(
		<>
			<Image 
				image={image}
				alternate={alternate}
			/>
			<Title 
				title={title}
			/>
			<Author 
				author={author}
			/>
		</>
	)
};

const Image = ({image, alternate})=>{
	// console.log(props);
	return <img src={image} alt={alternate}/>;
};

const Title = ({title})=>{
	// console.log(props);
	return <h3>{title}</h3>;
};

const Author = ({author})=>{
	// console.log(props);
	return <h5>{author}</h5>;
};

//ReactDom.render - This will mount the HelloWorld component to the root element
//<HelloWorld></HelloWorld> --> explicit version
ReactDOM.render(<BookList />,document.getElementById('root'));